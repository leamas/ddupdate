From: Stefano Rivera <stefano@rivera.za.net>
Date: Sun, 13 Nov 2022 10:07:43 +0200
Subject: Import setuptools before distutils

setuptools 60 uses its own bundled version of distutils, by default. It
injects this into sys.modules, at import time. So we need to make sure
that it is imported, before anything else imports distutils, to ensure
everything is using the same distutils version.

This change in setuptools is to prepare for Python 3.12, which will drop
distutils.

Bug-Debian: https://bugs.debian.org/1022527
Forwarded: https://github.com/leamas/ddupdate/pull/76
---
 setup.py | 5 ++---
 1 file changed, 2 insertions(+), 3 deletions(-)

diff --git a/setup.py b/setup.py
index b71778b..e8075c7 100644
--- a/setup.py
+++ b/setup.py
@@ -3,13 +3,12 @@
 import shutil
 import os
 import subprocess
+from glob import glob
 
+from setuptools import setup
 from distutils.command.clean import clean
 from distutils.command.install import install
 
-from glob import glob
-from setuptools import setup
-
 # pylint: disable=bad-continuation
 ROOT = os.path.dirname(__file__)
 ROOT = ROOT if ROOT else '.'
